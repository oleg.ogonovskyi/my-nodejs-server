const express = require('express');
const upload = require('express-fileupload');
const fs = require('fs');
const url = require('url');
const path = require('path');
const router = express.Router();

const app = express();

app.use(upload());
app.use(express.json());
app.use("/", router);


// get main page

router.get('/api', (req, res) => {
    res.sendFile(path.join(__dirname,'/index.html'));
});

// get all files

router.get('/api/files', (req, res) => {

    fs.readdir('./files', (err, files) => {
        if (err) {
            res.status(400).send({
                message: 'Client error',
            })
        } else {
            res.setHeader('Content-Type', 'application/json');
            res.status(200).send({
                message: 'Success',
                files: files.length > 0 ? files : 'There are no files available',
            }); 
        }  
    })

});

// get concrete file

router.get('/api/files/:filename', (req, res) => {

    let queryFile = url.parse(req.url, true).path.split('/').reverse()[0];
    let file;

    fs.readdir('./files', (err, files) => {
        if (err) {
            res.status(400).send({
                message: 'Client error',
            })
        } else {
            file = files.includes(queryFile);
        }
    });
    
    fs.readFile('./files/' + queryFile, 'utf-8', (err, data) => {

        if(!file) {
            res.status(500).send({
                message: `No file with ${queryFile} filename found`,  
            })
        } else if (err) {
            res.status(400).send({
                message: 'Client error',
            })
        } else {
            let date;
            fs.stat('./files/' + queryFile, (err, stats) => {
                res.setHeader('Content-Type', 'application/json');
                res.status(200).send({
                message: 'Success',
                filename: queryFile,
                content: data,
                extension: path.extname(queryFile),
                uploadedDate: stats.mtime
            });  
            });        
        }

    });

})

// post file

router.post('/api/files', (req, res) => {

    if (req.files) {
        let file = req.files.file;
        let fileName = file.name;

        file.mv('./files/' + fileName, err => {
            if (err) {
                res.status(400).send({
                    message: 'Server error'
                  });
            } else {
                res.status(200).send({
                    message: 'File created successfully',
                    fileName,
                });
            }
        })

    }
    
});

app.listen(8080);